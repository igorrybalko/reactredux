import React from 'react';
import { connect } from 'react-redux';
import { chchbox } from '../../actions';

class Checkbox extends React.Component{

    constructor(props) {
        super(props);

        this.props = props;
        console.log(this.props);
    }

    render(){
        return(

            <input
                checked={this.props.value}
                onChange={this.props.dochchbx}
                type="checkbox" />
        );
    }
}

Checkbox = connect(
    state => ({ value: state.reducers.changeCheckbox.marked }),
    dispatch => ({ dochchbx: () => dispatch(chchbox()) })
)(Checkbox);

// class Checkbox extends React.Component{
//     constructor(props){
//         super(props);
//
//         this.state = {
//             passed : 0
//         };
//
//         this.handleInputChange = this.handleInputChange.bind(this);
//
//     }
//
//     render(){
//         return (
//              <input
//                    onChange={this.handleInputChange}
//                  checked={this.state.passed}
//                type="checkbox" />
//         );
//     }
//
//     handleInputChange(e){
//         this.setState({ passed: !this.state.passed });
//     }
// }

class English extends React.Component{

    constructor(){
        super();

        this.state = {
            lessons : this.getListTasks()
        };

    }

    render() {
        return (
            <div className='EnglishPage'>

                <h1>English page</h1>
                <h2>Tasks</h2>

                <div>
                    <ul>
                        {this.getListTasks().map(el => {
                            return (<li key={el.id}>
                                <div className="checkbox">
                                    <label>
                                        <Checkbox test="qwerty"/>
                                        <span>{'Lesson ' + el.id}</span>
                                    </label>
                                </div>
                            </li>);
                        })}
                    </ul>
                </div>

            </div>
        );
    }


    getListTasks(){
        return ([
            {
                id: 1,
                passed: 0
            },
            {
                id: 2,
                passed: 1
            },
            {
                id: 3,
                passed: 0
            },
            {
                id: 4,
                passed: 0
            }
        ]);
    }
}

export default English;