import { Link, Route, Switch } from 'react-router-dom';
import React from 'react';
import axios from 'axios';
import English from './English.jsx';
import Reactjs from './Reactjs.jsx';

class FullCats extends React.Component{

    constructor(){
        super();

        this.state = {
            catList: []
        };
    }

    componentDidMount(){
        this.getCatList();
    }

    render() {

        return (

            <div className="catlist">

                <ul>
                    {
                        this.state.catList.map(cat => {
                            return <li key={cat.id}><Link to={cat.url}>{cat.name}</Link></li>
                        })
                    }
                </ul>
            </div>

        );
    }
    getCatList(){

        axios.get('/db/categories.json')
            .then( response => {
                this.setState({catList: response.data});
            })
            .catch( error => {
                console.log(error);
            });

    }
}

let Categories = () => {

    return (

        <Switch>
            <Route exact path='/categories' component={FullCats} />
            <Route path="/categories/english" component={English} />
            <Route path="/categories/reactjs" component={Reactjs} />
        </Switch>
    );

};

export default Categories;