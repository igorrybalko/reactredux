import React from 'react';

class Footer extends React.Component{
    render() {
        return (
            <footer>
                <div className="container">
                    &copy; copyright 2017
                </div>
            </footer>

        );
    }
}

export default Footer;