import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Categories from '../pages/Categories.jsx';
import Home from '../pages/Home.jsx';

class Main extends React.Component{
    render(){
        return(
            <main>
                <div className="container">
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/categories' component={Categories} />
                    </Switch>
                </div>
            </main>
        )
    }
}

export default Main;