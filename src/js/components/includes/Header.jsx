import React from 'react';
import { Link } from 'react-router-dom';

class Header extends React.Component{
    render() {
        return (
            <header>
                <div className="container">
                    <nav className="navbar navbar-inverse">
                        <ul className='nav navbar-nav'>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/categories">Categories</Link></li>
                        </ul>
                    </nav>
                </div>
            </header>
        );
    }
}

export default Header;