import { combineReducers } from 'redux';
import changeCheckbox from './changeCheckbox';

const reducers = combineReducers({
    changeCheckbox
});

export default reducers;