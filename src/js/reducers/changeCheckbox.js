const changeCheckbox = (state = {marked: false}, action) => {
    switch (action.type) {
        case 'CHCHBOX':
            console.log(state);
            return Object.assign({}, state, {marked: !state.marked});
        default:
            return state
    }
};

export default changeCheckbox;