import React from 'react';
import Header from './components/includes/Header.jsx';
import Footer from './components/includes/Footer.jsx';
import Main from './components/includes/Main.jsx';

import './styles/base.less';

class App extends React.Component{
    render() {
        return (
            <div>
                <Header />
                <Main />
                <Footer />
            </div>
        );
    }
}

export default App;