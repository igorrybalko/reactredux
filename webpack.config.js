'use strict';

const path = require('path');
const webpack = require('webpack');

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        app: './js/main.js'
    },
    output: {
        path: path.resolve(__dirname, './dist/js'),
        filename: 'bundle.js'
    },
    //devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.json']
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.jsx$/,
                loader: ["react-hot-loader", "babel-loader"],
                exclude: [/node_modules/, /dist/]
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader!autoprefixer-loader",
                exclude: [/node_modules/, /dist/]
            },
            {
                test: /\.less$/,
                use: [ 'style-loader', 'css-loader', 'less-loader', 'autoprefixer-loader' ],
                exclude: [/node_modules/, /dist/]
            }
        ]
    },
    // plugins: [
    //     new webpack.optimize.UglifyJsPlugin({
    //         compress: {
    //             warnings: false
    //         },
    //         //sourceMap: true
    //     })
    // ]
};