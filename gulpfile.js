'use strict';

let gulp     = require('gulp'),
    webpack  = require('webpack'),
    gutil    = require('gulp-util'),
    notifier = require('node-notifier'),
    path = require('path');

// let less = require('gulp-less'),
//     cleanCSS = require('gulp-clean-css'),
//     autoprefixer = require('gulp-autoprefixer'),
//     inject = require('gulp-inject');

let webpackConfig = require('./webpack.config.js');
let statsLog      = { // для красивых логов в консоли
    colors: true,
    reasons: true
};

function swallowError(error){
    console.log(error.toString());
    this.emit('end');
}

gulp.task('default', ['gulp_watch']);

gulp.task('gulp_watch', function () {
    //gulp.watch('./src/less/**/*.less', ['less']);
    gulp.watch('./src/js/**/*.js', ['scripts']);
    gulp.watch('./src/js/**/*.jsx', ['scripts']);
});

// gulp.task('less', function () {
//     return gulp.src('./dev/less/main.less')
//         .pipe(sourcemaps.init())
//         .pipe(less({
//             paths: [
//                 path.join(__dirname, 'less'),
//                 'bower_components'
//             ]
//         }))
//         .on('error', swallowError)
//         .pipe(autoprefixer({
//             browsers: ['last 5 versions', '> 5%'],
//             cascade: false
//         }))
//         .pipe(cleanCSS())
//         .pipe(rename('style.css'))
//         .pipe(sourcemaps.write('./'))
//         .pipe(gulp.dest('./dist/css'));
// });

gulp.task('scripts', (done) => {

    console.log(done);

    function onError(error) {
        let formatedError = new gutil.PluginError('webpack', error);

        notifier.notify({ // чисто чтобы сразу узнать об ошибке
            title: `Error: ${formatedError.plugin}`,
            message: formatedError.message
        });

        done(formatedError);
    }

    function onSuccess(detailInfo) {
        gutil.log('[webpack]', detailInfo);
        done();
    }

    function onComplete(error, stats) {
        if (error) { // кажется еще не сталкивался с этой ошибкой
            onError(error);
        } else if ( stats.hasErrors() ) { // ошибки в самой сборке, к примеру "не удалось найти модуль по заданному пути"
            onError( stats.toString(statsLog) );
        } else {
            onSuccess( stats.toString(statsLog) );
        }
    }

    // run webpack
    webpack(webpackConfig, onComplete);

});